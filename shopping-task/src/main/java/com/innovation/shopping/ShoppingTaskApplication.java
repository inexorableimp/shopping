package com.innovation.shopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingTaskApplication.class, args);
    }

}
