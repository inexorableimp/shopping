package com.innovation.shopping.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class TestModel implements Serializable {
    private String message;
}
